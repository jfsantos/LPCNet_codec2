#!/usr/bin/python3
# train_codec2.py
# Jean-Marc Valin & David Rowe
#
# Train a LPCNet model using Codec 2 features

'''
Usage:

  Given a 16 kHz training file all_speechcat.sw:

  $ ./codec2-dev/build_linux/unittest/16_8_short all_speechcat.sw all_speechcat_8k.sw
  $ ./codec2-dev/build_linux/src/c2sim all_speechcat_8k.sw --lspEWov all_speechcat_8k.f32
  $ ./train_codec2.py ~/Downloads/all_speechcat_8k.f32 ~/Downloads/all_speechcat.sw
'''

import lpcnet
import sys
import numpy as np
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from ulaw import ulaw2lin, lin2ulaw
import keras.backend as K
import h5py
import matplotlib.pyplot as plt
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto()

set_session(tf.Session(config=config))

# use this option to reserve GPU memory, e.g. for running more than
# one thing at a time.  Best to disable for GPUs with small memory
#config.gpu_options.per_process_gpu_memory_fraction = 0.5

nb_epochs  = 40
batch_size = 48 # Try reducing batch_size if you run out of memory on your GPU

Fs=8000
nb_lpc_order = 10          # lpc order
nb_udata_delay = 0         # Delay in features compared to centre of
                           # 10ms PCM frames checked with dl_align.m
nb_features = 23
nb_used_features = lpcnet.nb_used_features
frame_size = 80
feature_chunk_size = 15

model, _, _ = lpcnet.new_model(frame_size)
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['sparse_categorical_accuracy'])
model.summary()

feature_file = sys.argv[1] # 10 LSPs, Energy, Wo, Voicing, 10 LPCs
pcm_file = sys.argv[2]     # 16 bit unsigned short PCM samples
output_file_prefix=sys.argv[3]

# break up into "chunks", a kind of superframe

pcm_chunk_size = frame_size*feature_chunk_size

# u for unquantised, load 16 bit PCM samples delay to time align with
# features and convert to mu-law

udata = np.fromfile(pcm_file, dtype='int16')
udata = np.append(np.zeros(nb_udata_delay), udata)
data = lin2ulaw(udata)
nb_frames = int(len(data)/pcm_chunk_size)

# 10 LSPs, energy, Wo, voicing, LPCs

features = np.fromfile(feature_file, dtype='float32')

print("pcm frames: %d feature frames: %d" % (len(data)/frame_size, len(features)/nb_features))

# limit to discrete number of frames
      
data = data[:nb_frames*pcm_chunk_size]
udata = udata[:nb_frames*pcm_chunk_size]
features = features[:nb_frames*feature_chunk_size*nb_features]

features = np.reshape(features, (nb_frames*feature_chunk_size, nb_features))

features[:,10:11] = np.log10(features[:,10:11]) # want log energy
print(features[1:10,:10])
print(features[1:10,10:13])
print(features[1:10,13:nb_features])

#plt.figure(2)
#plt.plot(features[100:200,:10])
#plt.show()

# Noise injection: the idea is that the real system is going to be
# predicting samples based on previously predicted samples rather than
# from the original. Since the previously predicted samples aren't
# expected to be so good, noise is added to the training data.  Exactly
# how the noise is added makes a huge difference

in_data = np.concatenate([data[0:1], data[:-1]]);
noise = np.concatenate([np.zeros((len(data)*1//5)), np.random.randint(-3, 3, len(data)*1//5), np.random.randint(-2, 2, len(data)*1//5), np.random.randint(-1, 1, len(data)*2//5)])
in_data = in_data + noise
in_data = np.clip(in_data, 0, 255)

features = np.reshape(features, (nb_frames*feature_chunk_size, nb_features))

# Generate LPC prediction output upred[] and (in mu-law form) pred[]

upred = np.zeros(nb_frames*pcm_chunk_size)

print("\nGenerating LPC predicted speech ....")
pred_in = ulaw2lin(in_data)
for i in range(2, nb_frames*feature_chunk_size):
    upred[i*frame_size:(i+1)*frame_size] = 0
    for k in range(nb_lpc_order):
        upred[i*frame_size:(i+1)*frame_size] = upred[i*frame_size:(i+1)*frame_size] - \
            pred_in[i*frame_size-k:(i+1)*frame_size-k]*features[i, nb_features-nb_lpc_order+k]


'''
# plt some speech - useful to check the predictor is working and time alignment is good
st=1.2; en=1.3;
st_sam=int(st*Fs); en_sam=int(en*Fs);
plt.figure(1)
plt.plot(pred_in[st_sam:en_sam])
plt.plot(upred[st_sam:en_sam])
plt.plot((udata-upred)[st_sam:en_sam])
plt.show()
'''

pred = lin2ulaw(upred)

in_data = np.reshape(in_data, (nb_frames, pcm_chunk_size, 1))
in_data = in_data.astype('uint8')

# LPC residual, which is the difference between the input speech and
# the predictor output, with a slight time shift this is also the
# ideal excitation in_exc

out_data = lin2ulaw(udata-upred)
in_exc = np.concatenate([out_data[0:1], out_data[:-1]]);

out_data = np.reshape(out_data, (nb_frames, pcm_chunk_size, 1))
out_data = out_data.astype('uint8')

in_exc = np.reshape(in_exc, (nb_frames, pcm_chunk_size, 1))
in_exc = in_exc.astype('uint8')

pred = np.reshape(pred, (nb_frames, pcm_chunk_size, 1))
pred = pred.astype('uint8')

print("Normalising features.........\n");

# reshape to make normalisation easy

# normalise LSPs,log10(E), leave Wo for embedding below.  Note Wo also gets
# passed in as a float, not sure if this is necessary.  Voicing is already binary

means = features[:,:11].mean(axis=0); stds = features[:,:11].std(axis=0)
features[:,:11] -= means
features[:,:11] /= stds

# save means and stds to disk for decoder

np.savetxt(output_file_prefix+'_means.txt', means, fmt='%f')
np.savetxt(output_file_prefix+'_stds.txt', stds, fmt='%f')

print(means)
print(stds)

# reshape into shape that suits Conv1D, chunks of time series

features = np.reshape(features, (nb_frames, feature_chunk_size, nb_features))
features = features[:, :, :nb_used_features]
#plt.plot(features[8,:,:])
#plt.show()

# convert Wo to pitch period in range 0 .. 256 (should be 20 to 140)

print("Vectorizing pitch....\n")
Wo = features[:,:,11:12]
periods = (2*np.pi/Wo).astype('int16')
print(periods[1:100])

# don't feed pitch in any other way
features[:,:,11:12] = 0

# TODO - some plots to show time alignment of features, especially after reshape

in_data = np.concatenate([in_data, pred], axis=-1)

print("in_data pred in_exec features periods")
print(in_data.shape)
print(pred.shape)
print(in_exc.shape)
print(features.shape)
print(periods.shape)

# dump models to disk as we go
checkpoint = ModelCheckpoint(output_file_prefix+'_{epoch:02d}.h5')
 
model.compile(optimizer=Adam(0.001, amsgrad=True, decay=5e-5), loss='sparse_categorical_crossentropy', metrics=['sparse_categorical_accuracy'])
model.fit([in_data, in_exc, features, periods], out_data, batch_size=batch_size, epochs=nb_epochs, validation_split=0.0, callbacks=[checkpoint, lpcnet.Sparsify(2000, 40000, 400, 0.1)])
 
